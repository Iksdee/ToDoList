package pl.sda.utils;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.sda.entities.User;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;

@Component
public class PDFCreator {

    @Autowired
    SimpleDateFormat sdf;

    public File createPDF(User user) throws IOException, DocumentException {
        Document document = new Document();
        File file = new File(user.getEmail() + ".pdf");
        PdfWriter.getInstance(document, new FileOutputStream(file));
        document.open();
        Chapter chapter = this.getChapter(user);
        PdfPTable table = this.getTable(user);
        document.add(chapter);
        document.add(new Paragraph(" "));
        document.add(table);
        document.close();
        return file;
    }

    private Chapter getChapter(User user) {
        Font chapterFont = FontFactory.getFont(FontFactory.TIMES_BOLD, 16, Font.BOLDITALIC);
        Chunk chunk = new Chunk(this.getChapterTitle(user), chapterFont);
        Chapter chapter = new Chapter(new Paragraph(chunk), 1);
        chapter.setNumberDepth(0);
        return chapter;
    }

    private String getChapterTitle(User user) {
        StringBuffer sb = new StringBuffer();
        sb.append("Wykaz zadan dla ");
        sb.append(user.getName());
        sb.append(" ");
        sb.append(user.getSurname());
        sb.append(", e-mail: ");
        sb.append(user.getEmail());
        return sb.toString();
    }

    private PdfPTable getTable(User user) {
        PdfPTable table = new PdfPTable(4);
        table.addCell("ZADANIA");
        table.addCell("POCZĄTEK");
        table.addCell("KONIEC");
        table.addCell("STATUS");
        user.getTask().stream().forEach(t -> {
            table.addCell(t.getDescription());
            table.addCell(sdf.format(t.getStartData()));
            table.addCell(sdf.format(t.getEndData()));
            if (t.isStatus()) {
                table.addCell("DONE");
            } else {
                table.addCell("IN PROGRESS");
            }
        });
        return table;
    }
}

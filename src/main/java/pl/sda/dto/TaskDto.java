package pl.sda.dto;

public class TaskDto {
    private Integer id;
    private String description;
    private String startData;
    private String endData;
    private boolean status;

    public TaskDto() {
    }

    public TaskDto(Integer id, String description, String startData, String endData, boolean status) {
        this.id = id;
        this.description = description;
        this.startData = startData;
        this.endData = endData;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStartData() {
        return startData;
    }

    public void setStartData(String startData) {
        this.startData = startData;
    }

    public String getEndData() {
        return endData;
    }

    public void setEndData(String endData) {
        this.endData = endData;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}

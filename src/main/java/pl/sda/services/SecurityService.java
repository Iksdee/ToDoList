package pl.sda.services;

public interface SecurityService {
    String findLoggedInEmail();

    void autoLogin(String email, String password);

    String findLoggedInName();
}

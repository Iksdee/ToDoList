package pl.sda.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import pl.sda.dto.UserCreateForm;
import pl.sda.entities.Email;
import pl.sda.entities.User;

@Service
public class NotyficationService {

    private JavaMailSender javaMailSender;

    @Autowired
    public NotyficationService(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }


    public void sendNotyfication(UserCreateForm user) throws MailException {
        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setTo(user.getEmail());
        mail.setFrom("javaktw@gmail.com");
        mail.setSubject("Zespoł JawaKtw - Rejestracja");
        mail.setText("Rejestracja użytkownika " + user.getName() + " " + user.getSurname() + " przebiegła pomyślnie, dziękujemy");
        javaMailSender.send(mail);
    }

    public void sendMailToUs(Email email) throws MailException {
        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setTo(email.getTo());
        mail.setFrom(email.getFrom());
        mail.setSubject(email.getSubject() + " mail: "+ email.getFrom());
        mail.setText(email.getText());
        javaMailSender.send(mail);
    }
}
package pl.sda.services;

import org.springframework.ui.ModelMap;
import pl.sda.dto.TaskDto;
import pl.sda.entities.Task;

import java.io.File;
import java.util.List;

public interface TaskService {
    void saveTask(TaskDto taskDto);

    List<TaskDto> getTasksByUser();

    void delete(Integer id);

    TaskDto getTaskDto(Integer id);

    TaskDto createTaskDto(Task task);

    void setTaskDone(Integer id);

    File getFileWithTasks(Integer typeOfExport);

    String getContentType(Integer typeOfExport);

}

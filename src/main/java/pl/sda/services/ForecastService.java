package pl.sda.services;

import pl.sda.dto.ForecastDto;

public interface ForecastService {
    ForecastDto getForecastDto(String cityName);
}

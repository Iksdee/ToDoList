package pl.sda.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import pl.sda.dto.TaskDto;
import pl.sda.entities.Email;
import pl.sda.services.NotyficationService;

@Controller
public class InfoController {

    @Autowired
    NotyficationService notyficationService;


    @GetMapping("/info")
    public String showInfoPage() {
        return "info/info";
    }


    @GetMapping("/contact")
    public ModelAndView sendEmail(ModelMap map) {
        map.addAttribute("sendEmail", new Email());
        return new ModelAndView("info/contact", map);
    }


    @PostMapping("/contact")
    public ModelAndView sendEmailToUs(@ModelAttribute Email email, ModelMap map) {

        try{
            notyficationService.sendMailToUs(email);
        }catch (MailException e){
            System.out.println("Błąd w wysyłaniu wiadomości mail");
        }
        map.addAttribute("sendEmail", new Email());

        return new ModelAndView("info/contact", map);
    }

}

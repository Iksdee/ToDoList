package pl.sda.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import pl.sda.entities.Role;
import pl.sda.entities.User;
import pl.sda.repositories.RoleRepository;
import pl.sda.repositories.UserRepository;
import pl.sda.services.SecurityService;
import pl.sda.services.UserService;

import java.util.HashSet;

/**
 * Created by MURZON on 2017-04-05.
 */
@Controller
public class AdminController {

    @Autowired
    private UserService userService;

    @Autowired
    private SecurityService securityService;

    @GetMapping("/admin/users")
    public ModelAndView showUsers(ModelMap modelMap) {
        modelMap.addAttribute("actualUserEmail",securityService.findLoggedInEmail());
        modelMap.addAttribute("userList",userService.getUserList());
        return new ModelAndView("/admin/users",modelMap);
    }

    @GetMapping("/user/delete")
    public String deleteUser(@RequestParam("email") String email){
        if (securityService.findLoggedInEmail().equals(email)) {
            return "redirect:/admin/users";
        }
        userService.deleteUser(email);
        return "redirect:/admin/users";
    }

    @GetMapping("/admin/addAdminRole")
    public ModelAndView addAdminRole(@RequestParam("id") Integer id){
        userService.addAdminRole(id);
        return new ModelAndView("redirect:/admin/users");
    }

}
